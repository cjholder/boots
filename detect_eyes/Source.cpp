#include <opencv.hpp>
#include <iostream>
#include <opencv2/ml.hpp>
#include <Windows.h>

using namespace cv;
using namespace cv::ml;
using namespace std;

bool test = 1;
bool train = 0;
int hsizex = 48;
int hsizey = 32;
int fnum = 540;

double testScale = 0.024;

vector<string>dirs = {"E:/eyes/T102898_1/", "E:/eyes/T102898_2/", "E:/eyes/T102898_3/"};
string testDir = "E:/E101045/images/";
string outDir = "E:/eyes/E101045/";
string svm_file = "E:/eyes/SVM.svm";

void getData(Mat&, Mat&);
vector<string> GetFiles(string f);
vector<Rect> FindPeaks(Mat img);
bool overlaps(Rect r1, Rect r2);


int main()
{
	Size hsize(hsizex, hsizey);

	Mat data_samples, labelsMat;
	vector<Mat> crops;

	Ptr<ml::SVM> svm;

	if (train)
	{
		getData(data_samples, labelsMat);

		svm = SVM::create();
		svm->setType(ml::SVM::ONE_CLASS);
		svm->setKernel(ml::SVM::LINEAR);
		svm->setDegree(3);
		svm->setGamma(1);
		svm->setCoef0(0);
		svm->setC(1);
		svm->setNu(0.5);
		svm->setP(0);
		svm->setTermCriteria(cvTermCriteria(CV_TERMCRIT_ITER, 5000, FLT_EPSILON));
		
		svm->train(data_samples, ml::ROW_SAMPLE, labelsMat);

		svm->save(svm_file);
		cout << endl << "SVM saved";

	}
	else
	{
		cout << endl << "loading SVM: " << svm_file;
		svm = SVM::load(svm_file);
	}

	if (test)
	{
		cout << endl << "Testing...";

		vector<string>fnames = GetFiles(testDir);

		for (int ii = 0; ii < fnames.size(); ii += 1)
		{
			int f = fnames[ii].find("FO");
			if (f > 0)			
				f = fnames[ii].find("t0");			

			if (f > 0)
			{
				cout << endl << fnames[ii];


				int sp = fnames[ii].find("_");
				string vol = fnames[ii].substr(1, sp-1).c_str();
				string s2 = fnames[ii].substr(sp + 1);

				sp = s2.find("_");
				s2 = s2.substr(sp + 1);

				sp = s2.find("_");
				string day = s2.substr(0, sp).c_str();


				Mat iTF(imread(testDir + fnames[ii]));
				if (iTF.rows > 0)
				{
					Mat iT2, iT;
					resize(iTF, iT2, Size(0, 0), testScale * 4, testScale * 4);
					resize(iTF, iT, Size(0, 0), testScale, testScale);

					HOGDescriptor hog;
					hog.winSize = Size(hsizex, hsizey);
					vector<float>hogf;
					hog.compute(iT, hogf, Size(1, 1), Size(0, 0));
					Mat hogs = Mat(Size(fnum, hogf.size() / fnum), CV_32FC1, &hogf[0]);
					int hog_win_step = (iT.cols - (hog.winSize.width - 1)) / 1;

					cout << endl << "HOG features computed";

					Mat heat(iT2.size(), CV_8U, Scalar(0));

					for (int i = 0; i < hogs.rows; ++i)
					{
						int x = (i % hog_win_step) * 1;
						int y = (i / hog_win_step) * 1;

						Mat res = Mat();
						svm->predict(hogs(Rect(0, i, hogs.cols, 1)), res, ml::StatModel::RAW_OUTPUT);

						double v = max(0, min(255, (res.at<float>(0, 0) + 1000) / 128));
						double mx = (x * 4) + (hsizex * 2);
						double my = (y * 4) + (hsizey * 2);
						double rs = (hsizex * 2 * hsizex * 2) + (hsizey * 2 * hsizey * 2);

						for (int yy = y * 4; yy < (y * 4) + (hsizey * 4); ++yy)
						{
							for (int xx = x * 4; xx < (x * 4) + (hsizex * 4); ++xx)
							{
								double d = ((mx - xx) * (mx - xx)) + ((my - yy) * (my - yy));
								double nv = heat.at<uchar>(yy, xx) + (v * (1 - (d / rs)));
								heat.at<uchar>(yy, xx) = min(255, nv);
							}
						}
					}

					vector<Rect>dets = FindPeaks(heat);

					Rect rL(dets[0].x / testScale, dets[0].y / testScale, dets[0].width / testScale, dets[0].height / testScale);
					Rect rR(dets[1].x / testScale, dets[1].y / testScale, dets[1].width / testScale, dets[1].height / testScale);
					if (rL.x < rR.x)
					{
						Rect rT = rR;
						rR = rL;
						rL = rT;
					}
					Mat iL(iTF(rL));
					Mat iR(iTF(rR));
					imwrite(outDir + vol + "L.png", iL);
					imwrite(outDir + vol + "R.png", iR);

					rectangle(iT2, Rect(dets[0].x * 4, dets[0].y * 4, dets[0].width * 4, dets[0].height * 4), Scalar(255, 0, 0));
					rectangle(iT2, Rect(dets[1].x * 4, dets[1].y * 4, dets[1].width * 4, dets[1].height * 4), Scalar(255, 0, 0));
					imshow("heat", heat);
					imshow("it2", iT2);
					waitKey(1);
				}
			}
		}
	}
	cout << endl << "finished";
	getchar();
}




void getData(Mat& data_samples, Mat& labelsMat)
{
	vector<string>fnames = {};
	
	for (int i = 0; i < dirs.size(); ++i)
	{
		vector<string> fn = GetFiles(dirs[i]);
		for (int j = 0; j < fn.size(); ++j)
		{
			if (fn[j].substr(fn[j].size() - 4) == ".png")
				fnames.push_back(dirs[i] + fn[j]);
		}
	}

	data_samples = Mat(fnames.size() * 2, fnum, CV_32FC1, Scalar(0));
	labelsMat = Mat(fnames.size() * 2, 1, CV_32SC1, Scalar(0));
	
	Mat allboxes;

	for (int i = 0; i < fnames.size(); ++i)
	{
		Mat img(imread(fnames[i]));

		HOGDescriptor hog;		
		hog.winSize = Size(hsizex, hsizey);
		vector<Point> pos = { Point(hog.winSize.width / 2, hog.winSize.height / 2) };

		Mat bb, bbm;
		resize(img, bb, hog.winSize);
		flip(bb, bbm, 1);

		cvtColor(bb, bb, CV_BGR2GRAY);
		cvtColor(bbm, bbm, CV_BGR2GRAY);

		vector<float> desc, descm;
		hog.compute(bb, desc, Size(1, 1), Size(0, 0));//, posss);
		hog.compute(bbm, descm);
		
		for (int j = 0; j < desc.size(); ++j)
		{
			data_samples.at<float>(i * 2, j) = desc[j];
			data_samples.at<float>((i * 2) + 1, j) = descm[j];
		}

		labelsMat.at<short>(i * 2, 0) = 1;
		labelsMat.at<short>((i * 2) + 1, 0) = 1;


		if (i % 20 == 0)
			cout << endl << i << " of " << fnames.size();
	}
	cout << endl << fnames.size() * 2 << " data samples created";
}

vector<string> GetFiles(string f)
{
	vector<string> fNames;
	HANDLE hFind;
	WIN32_FIND_DATAA ffd;
	hFind = FindFirstFileA((f + "*").c_str(), &ffd);
	int count = 0;
	while (true)
	{
		if (hFind == INVALID_HANDLE_VALUE)
		{
			break;
		}
		if (strcmp(ffd.cFileName, ".") && strcmp(ffd.cFileName, ".."))
		{
			string f = ffd.cFileName;
			fNames.push_back(f);
		}
		if (FindNextFileA(hFind, &ffd) == 0)
			break;
	}
	return fNames;
}

vector<Rect> FindPeaks(Mat img)
{
	resize(img, img, Size(0, 0), 0.25, 0.25);

	long mx1 = 0;
	long mx2 = 0;
	Rect mr1(0, 0, 0, 0);
	Rect mr2(0, 0, 0, 0);

	for (int y = 0; y < img.rows - hsizey; ++y)
	{
		for (int x = 0; x < img.cols - hsizex; ++x)
		{
			Rect r(x, y, hsizex, hsizey);
			long tot = 0;
			for (int yy = y; yy < y + hsizey; ++yy)
			{
				for (int xx = x; xx < x + hsizex; ++xx)
				{
					tot += img.at<uchar>(yy, xx);
				}
			}

			if (tot > mx1)
			{
				if (overlaps(r, mr1))
				{
					mx1 = tot;
					mr1 = r;
					if (overlaps(r, mr2))
					{
						mx2 = 0;
						mr2 = Rect(0, 0, 0, 0);
					}
				}
				else
				{
					mx2 = mx1;
					mr2 = mr1;
					mx1 = tot;
					mr1 = r;
				}				
			}
			else if (tot > mx2)
			{
				if (!overlaps(r, mr1))
				{
					mr2 = r;
					mx2 = tot;
				}
			}
		}
	}
	return{ mr1, mr2 };
}

bool overlaps(Rect r1, Rect r2)
{
	if (r1.x < r2.x + r2.width && r2.x < r1.x + r1.width)
	{
		if (r1.y < r2.y + r2.height && r2.y < r1.y + r1.height)
		{
			return true;
		}
	}
	return false;
}
