import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import scipy.misc as misc
import csv
import os
import os.path
import random
from netdef import Net
import argparse
import matplotlib.pyplot as plt

def load_data(ii, images, samples):
    vol = images[ii][images[ii].rfind('/') + 1 : -4]        
    ti = -1
    for j in range(int(vol), len(samples)):
        if vol == samples[j]['id']:
            ti = j
            break            
        
    if ti >= 0:                
        targets = (samples[ti]['age'], samples[ti]['angle'], samples[ti]['count'], samples[ti]['length'])
    else:
        print("data for sample " + str(ii) + " not found")
        return -1

    imgs0 = np.zeros((1, 3, showsize[0], showsize[1]))
    target = np.zeros((1, 4))
    imm = 0
    ims = 0

    img = misc.imread(images[ii])
    imgs0[0,:] = (misc.imresize(img, showsize).transpose(2,0,1))
    im = misc.imresize(img, imsize).transpose(2,0,1)[np.newaxis].astype(np.float32)
    
    imm = im.mean()
    im = im - im.mean()
    ims = im.std()
    im = im / im.std()
    
    intup = (im,)
    for jj in range(4):
        target[0][jj] = min(float(targets[jj]) / maxes[jj], 1.0)
    
    inp = np.concatenate(intup, axis = 0)
    
    return (torch.from_numpy(inp.astype(np.float32)).to(device), torch.from_numpy(target.astype(np.float32)).to(device), ii, imgs0)

print("checking arguments")

ap = argparse.ArgumentParser()
ap.add_argument("--imdir", type=str, required=True)
ap.add_argument("--model", type=str, required=True)
ap.add_argument("--arch", type=str, required=False, default="VGG")
ap.add_argument("--imX", type=int, required=False, default=256)
args = ap.parse_args()

showout = False

netarch = args.arch
imsize = (args.imX, int(args.imX * 1.5))
showsize = (65,96)
imdir = args.imdir
datafile = imdir + "data.csv"

features = ["age", "angle", "count", "length"]
maxes = [80.0, 70.0, 200.0, 15.0]

print("reading data file: " + datafile)
samples =[]
f = open(datafile, 'r', encoding='utf-8-sig')
csvreader = csv.DictReader(f)
for row in csvreader:
    samples.append(row)

images = []
files = os.listdir(imdir)
for i in range(len(files)):
    if files[i][-4:] == ".png":        
        images.append(imdir + files[i])

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

net = Net(netarch)
print("loading model: " + args.model)
net.load_state_dict(torch.load(args.model, map_location=str(device)))

net.to(device)

print("testing using " + str(device))
print("architecture: " + netarch)
print("image size: (" + str(imsize[0]) + ", " + str(imsize[1]) + ")")

ii = 0

errors = [[],[],[],[]]

for i in range(len(images)):    
    if i%10 == 0:
        print("testing image " + str(i) + " of " + str(len(images)))
    (inp, target, ii2, imgs0) = load_data(i, images, samples)
    
    output = net(inp)   
    
    if showout:
        print("")        
    for j in range(4):
        err = min(1.0, abs(output[0,j].item() - target[0,j].item()) / target[0,j].item())
        errors[j].append(err)

        if showout:
            print(features[j])
            print("Actual: " + str(target[0,j].item() * maxes[j]) + ", Predicted: " + str(output[0,j].item() * maxes[j]))

    if showout:
        plt.imshow(misc.imread(images[i]))
        plt.show()

#hists=np.zeros((4,10))

for j in range(4):
    av = 0.0
    for i in range(len(errors[j])):
        av = av + errors[j][i]
        #hists[j, int(errors[j][i] * 10)] = hists[j, int(errors[j][i] * 10)] + 1
    av = av / float(len(errors[j]))
    print("average " + features[j] + " error: " + str(av*100) + "%")

plt.subplot(221)
plt.hist(errors[0], 10)
plt.title(features[0])
plt.xlabel("error")

plt.subplot(222)
plt.hist(errors[1], 10)
plt.title(features[1])
plt.xlabel("error")

plt.subplot(223)
plt.hist(errors[2], 10)
plt.title(features[2])
plt.xlabel("error")

plt.subplot(224)
plt.hist(errors[3], 10)
plt.title(features[3])
plt.xlabel("error")

plt.show()
