import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import scipy.misc as misc
import csv
import os
import os.path
import random
import visdom
from netdef import Net
import argparse

def load_data(batchsize, ii, images, samples):
    imgs = []
    targets = []
    
    while len(imgs) < batchsize:  
        vol = images[ii][images[ii].rfind('/') + 1 : -4]        
        ti = -1
        for j in range(int(vol), len(samples)):
            if vol == samples[j]['id']:
                ti = j
                break            
            if len(samples[j]['id']) > 0:
                if int(samples[j]['id']) > int(vol):
                    break

        if ti >= 0:
            if float(samples[ti]['angle']) * float(samples[ti]['length']) * float(samples[ti]['count']) * float(samples[ti]['age']) > 0:
                imgs.append(images[ii])
                targets.append((samples[ti]['age'], samples[ti]['angle'], samples[ti]['count'], samples[ti]['length']))
        
        ii = ii + 1
        if ii >= len(images):
            ii = 0
        
    intup = ()    
    imgs0 = np.zeros((batchsize, 3, showsize[0], showsize[1]))
    target = np.zeros((batchsize, 4))
    imm = 0
    ims = 0
    for j in range(batchsize):
        img = misc.imread(imgs[j])
        imgs0[j,:] = (misc.imresize(img, showsize).transpose(2,0,1))
        im = misc.imresize(img, imsize).transpose(2,0,1)[np.newaxis].astype(np.float32)
        
        imm = im.mean()
        im = im - im.mean()
        
        ims = im.std()
        im = im / im.std()
        if random.randint(0,1):
            im = np.flip(im, 3)
        intup = intup + (im,)
        for jj in range(4):
            target[j][jj] = min(np.float32(targets[j][jj]) / maxes[jj], 1.0)
    inp = np.concatenate(intup, axis = 0)
    
    return (torch.from_numpy(inp.astype(np.float32)).to(device), torch.from_numpy(target.astype(np.float32)).to(device), ii, imgs0)

print("checking arguments")

ap = argparse.ArgumentParser()
ap.add_argument("--imdir", type=str, required=True)
ap.add_argument("--arch", type=str, required=False, default="VGG")
ap.add_argument("--imX", type=int, required=False, default=256)
ap.add_argument("--iter", type=int, required=False, default=5000)
ap.add_argument("--vis", type=int, required=False, default=0)
ap.add_argument("--batchsize", type=int, required=False, default=4)
ap.add_argument("--ksample", type=int, required=False, default=0)
ap.add_argument("--snapshot", type=str, required=False, default="")
ap.add_argument("--previters", type=int, required=False, default=0)
args = ap.parse_args()

#netarch = "Alexnet"
netarch = args.arch
imsize = (args.imX, int(args.imX * 1.5))
showsize = (65,96)
#imdir = "E:/eyes/combined/"
imdir = args.imdir
datafile = imdir + "data.csv"
numiters = args.iter
valfreq = 100
ssfreq = 500
visConnected = args.vis
batchsize = args.batchsize
kval = args.ksample

ssdir = "./snapshots_" + netarch + "_" + str(imsize[0]) + "_" + str(batchsize) + "_" + str(kval) + "/"
if not os.path.exists(ssdir):
    print("creating directory: " + ssdir)
    os.makedirs(ssdir)
else:
    print("directory already exists: " + ssdir)

features = ["age", "angle", "count", "length"]
maxes = [80.0, 70.0, 200.0, 15.0]

vis = visdom.Visdom()

print("reading data file: " + datafile)
samples =[]
f = open(datafile, 'r', encoding='utf-8-sig')
csvreader = csv.DictReader(f)
for row in csvreader:
    samples.append(row)

net = Net(netarch)

if args.snapshot != "":
    print("loading model: " + args.snapshot)
    net.load_state_dict(torch.load(args.snapshot))

#optimiser = optim.SGD(net.parameters(), lr=lr, momentum=0.9)
optimiser = optim.Adadelta(net.parameters())
criterion = nn.MSELoss()

images = []
imVal = []
files = os.listdir(imdir)
for i in range(len(files)):
    if files[i][-4:] == ".png":
        if i % 10 == kval:
            imVal.append(imdir + files[i])
        else:
            images.append(imdir + files[i])

random.shuffle(images)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net.to(device)

print("training using " + str(device))
print("architecture: " + netarch)
print("batch size: " + str(batchsize))
print("image size: (" + str(imsize[0]) + ", " + str(imsize[1]) + ")")
print("k-fold cross validation subsample: " + str(kval))


ii = 0
iiV = 0
epochs = 1
for i in range(args.previters+1, numiters):    
    #print("iteration " + str(i))
    optimiser.zero_grad()

    
    (inp, target, ii2, imgs0) = load_data(batchsize, ii, images, samples)
    if ii2 < ii:
        epochs = epochs + 1
        random.shuffle(images)
    ii = ii2

    if visConnected:
            try:
                if i == 0:
                    showeyes = vis.images(imgs0)
                else:
                    showeyes = vis.images(imgs0, win=showeyes)
            except:
                visConnected = False     

    output = net(inp)   
    loss = criterion(output, target)
    loss.backward()
    optimiser.step()
  
    if i % valfreq == 0:
        (inpV, targetV, iiV, imgs0) = load_data(batchsize, iiV, imVal, samples)
        outputV = net(inpV)   
        lossV = criterion(outputV, targetV)
        print("\nepoch " + str(epochs))
        print("iteration " + str(i + 1))        
        print("Training Loss:   " + str(round(loss.item(), 4)))
        print("Validation Loss: " + str(round(lossV.item(), 4)))
        rn = random.randint(0, batchsize-1)
        print("actual values for validation sample " + str(rn))
        print(targetV[rn])
        print("predicted values for validation sample " + str(rn))
        print(outputV[rn])
        
        if visConnected:
            try:                
                x = np.array([[i, i]])
                y = np.array([[loss.item(), lossV.item()]])
                if i == 0:                        
                    losswin = vis.line(Y=y, X=x, opts=dict(legend = ["Training", "Validation"], showlegend=True))
                else:
                    losswin = vis.line(Y=y, X=x, win=losswin, update="append", opts=dict(legend = ["Training", "Validation"], showlegend=True))
            except:
                visConnected = False                
    
    if (i + 1) % ssfreq == 0 or i == numiters - 1:
        #torch.save(net.state_dict(), "./snapshots/" + netarch + "_iter_" + str(i+1))
        torch.save(net.state_dict(), ssdir + "_iter_" + str(i+1))
